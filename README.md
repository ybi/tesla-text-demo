# TESLA Text Experiments Demo
Some Experiments on TESLA protocol, which is forked from [unideb-tesla/tesla-text-demo repository at Github](https://github.com/unideb-tesla/tesla-text-demo)

-------
# Original README
An example text broadcasting server-client application based on the TESLA protocol.

This project includes a simple client-server application, that we used to test the TESLA protocol with. We simply broadcast text messages, and basically do nothing special with them on the client side, just display them. This project is just a proof of work. We recommend to open it with your favorite IDE, like IntelliJ, and run the project from it.
